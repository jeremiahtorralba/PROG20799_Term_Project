#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FLUSH stdin=freopen(NULL,"r",stdin)

struct bankAccount {
    int account_number;
    char *account_name;
    double account_balance;
    struct bankAccount *nextPtr;
};

typedef struct bankAccount Account;
typedef struct bankAccount *AccountPtr;

AccountPtr push(AccountPtr topPtr, int account_number,
        char account_name[], double account_balance);
void printStack(AccountPtr currentPtr);
int isEmpty(AccountPtr topPtr);

int main(void) {
    AccountPtr accountPtr = NULL; // bank account pointer
    int choice; // user choice

    int account_number;
    char account_name[50];
    double account_balance;

    printf("Enter account number: ");
    scanf("%d", &account_number);

    FLUSH;

    printf("Enter account name: ");
    fgets(account_name, sizeof (account_name), stdin);
    account_name[strcspn(account_name, "\n")] = '\0';

    FLUSH;

    printf("Enter account balance: ");
    scanf("%lf", &account_balance);

    accountPtr = push(accountPtr, account_number,
            account_name, account_balance);

    printStack(accountPtr);
    
    return 0;
}

/*
    push() function created by Arel Jann Clemente.
*/
AccountPtr push(AccountPtr topPtr, int account_number,
        char account_name[], double account_balance) {
    // create a new pointer
    AccountPtr newPtr = (AccountPtr) malloc(sizeof (Account));
    // check if memory allocation for new pointer is successful
    if (newPtr != NULL) {
        // assign account number to new account
        newPtr->account_number = account_number;
        // copy account name to new account
        newPtr->account_name = (char*) malloc(sizeof(char)*strlen(account_name));
        strncpy(newPtr->account_name, account_name, strlen(account_name) + 1);
        // assign account balance to new account
        newPtr->account_balance = account_balance;
        // assign top pointer to new account's next pointer 
        newPtr->nextPtr = topPtr;
        // assign new account to top pointer
        topPtr = newPtr;
    } else {
        printf("Unable to insert account. No memory available.\n");
    }
    return topPtr;
}

/*
    printStack() function created by Arel Jann Clemente.
*/
void printStack(AccountPtr currentPtr) {
    if (currentPtr == NULL) {
        printf("The stack is empty.\n\n");
    } else {
        printf("The stack is:\n");
        while (currentPtr != NULL) {
            printf("Account Number: %d\nAccount Name: %s\nAccount Balance: %.2lf\n",
                    currentPtr->account_number,
                    currentPtr->account_name,
                    currentPtr->account_balance);
            currentPtr = currentPtr->nextPtr;
        }
        //printf("\nNULL\n\n");
    }
}

int isEmpty(AccountPtr topPtr) {
    return topPtr == NULL;
}